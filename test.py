# Test case 1 code implimentation: Search for node
from playwright.sync_api import Page

def remove_version_from_url(url):
    url_parts = url.split("/")
    pkg_name = url_parts[4].split("-")[0]
    url_parts[4] = pkg_name
    return "/".join(url_parts)

def test_search_and_documentation(page: Page):
    # 1. Open website https://snapcraft.io/
    page.goto("https://snapcraft.io/")

    # 2. Search field is present
    search_field = page.locator("input[id=search-input]")
    assert search_field.is_visible(), "Search field is not visible."

    # 3. Enter 'node' text in search field
    search_field.fill("node")

    # 4. Press enter
    page.keyboard.press("Enter")

    # 5. Press search button
    page.click('button.p-button')

    # 6. Search field still in page
    assert search_field.is_visible(), "Search field is not visible."

    # 7. The result of search present
    page.wait_for_selector(".u-fixed-width", timeout=3000)
