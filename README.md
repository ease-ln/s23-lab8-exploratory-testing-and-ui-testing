## Website tested: https://snapcraft.io/

### Test case 1: Search for node
| Step Number | What Done                                              | Status | Comment                                                       |
|-------------|--------------------------------------------------------|--------|---------------------------------------------------------------|
| 1           | Open website https://snapcraft.io/                     | Pass   | The page opened                                               |
| 2           | Search field is present                                | Pass   | The search field is present                                   |
| 3           | Enter 'node' text in search field                      | Pass   | Enter 'node' text in search field                             |
| 4           | Press enter                                            | Pass   | The search works for Enter button press from keyboard         |
| 5           | Press search button                                    | Pass   | The search works for Search button press                      |
| 6           | Search field still in page                             | Pass   | The search field still on page => user can continue search    |
| 3           | The result of search present                           | Pass   | Search results are presented                                  |

### Test case 2: Navigation check
| Step Number | What Done                                              | Status | Comment                                                       |
|-------------|--------------------------------------------------------|--------|---------------------------------------------------------------|
| 1           | Open website https://snapcraft.io/                     | Pass   | The page opened                                               |
| 2           | 'About' button is present                              | Pass   | 'About' button is present                                     |
| 3           | Press 'About' button                                   | Pass   | Clicked 'About' button                                        |
| 4           | About page opened                                      | Pass   | About page opened                                             |
| 5           | 'Snap Store' button is present                         | Pass   | 'Snap Store' button is present                                |
| 6           | Press 'Snap Store' button                              | Pass   | Clicked 'Snap Store' button                                   |
| 7           | Snap Store page opened                                 | Pass   | Snap Store page opened                                        |

### Test case 3: Check app page (node)
| Step Number | What Done                                              | Status | Comment                                                       |
|-------------|--------------------------------------------------------|--------|---------------------------------------------------------------|
| 1           | Open website https://snapcraft.io/                     | Pass   | The page opened                                               |
| 2           | Title is present                                       | Pass   | Title is present                                              |
| 3           | Press button for version                               | Pass   | Clicked button for version                                    |
| 4           | Press 'install' button                                 | Pass   | Clicked 'install' button                                      |
| 5           | Installation command line present                      | Pass   | Installation command line present   b                         |
| 6           | 'Get set up for snaps.' link click                     | Pass   | Clicked 'Get set up for snaps.' lin                           |
| 7           | 'Installing snapd' page opened                         | Pass   | Installing snapd page opened                                  |